﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Entrez le nombre de seconde à convertir : ");
            string Seconde = Console.ReadLine();
            int nombreSecondes = int.Parse(Seconde);
            int nbHeures = nombreSecondes / 3600;
            nombreSecondes = nombreSecondes % 3600;
            int nbMinutes = nombreSecondes / 60;
            nombreSecondes = nombreSecondes % 60;
            int nbSec = nombreSecondes;
            Console.WriteLine($"Le nombre de {Seconde} correspond à {nbHeures} heure(s) {nbMinutes} minute(s) {nbSec} secondes.");
            Console.ReadKey();

        }
    }
}
