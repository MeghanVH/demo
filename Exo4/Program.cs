﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo4
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, int> bar = new Dictionary<string, int>();
            while (true)
            {
                Console.WriteLine("1. Ajouter");
                Console.WriteLine("2.Modifier");
                Console.WriteLine("3.Supprimer");
                int choix = int.Parse(Console.ReadLine());
                if (choix == 1)
                {
                    Console.WriteLine("Quel produit souhaitez-vous ajouter ?");
                    string reponse = Console.ReadLine();
                    reponse = reponse.ToUpper();
                    while (bar.ContainsKey(reponse))
                    {
                        Console.WriteLine("Le produit existe déjà");
                        reponse = Console.ReadLine();
                    }
                    Console.WriteLine($"Quelle quantité de {reponse} désirez-vous :");
                    string val = Console.ReadLine();
                    int nb;
                    while (!int.TryParse(val, out nb))
                    {
                        Console.WriteLine("Quantité non valide");
                        val = Console.ReadLine();
                    }
                    bar.Add(reponse, nb);
                }
                else if (choix == 2)
                {
                    Console.WriteLine("Quel produit souhaitez-vous modifier ?");
                    string reponse = Console.ReadLine();
                    reponse = reponse.ToUpper();
                    if (bar.ContainsKey(reponse))
                    {
                        Console.WriteLine("Quelle est la nouvelle quantité ?");
                        string rep = Console.ReadLine();
                        int quantite = int.Parse(rep);
                        bar[reponse] = quantite;
                    }
                }
                else if (choix == 3)
                {
                    Console.WriteLine("Quel est le produit à supprimer ?");
                    string reponse = Console.ReadLine();
                    reponse = reponse.ToUpper();
                    bar.Remove(reponse);
                }

                Console.Clear();
                Console.WriteLine("Etat du stock :");
                foreach (KeyValuePair<string, int> kvp in bar)
                {
                    Console.WriteLine($"{kvp.Key} : {kvp.Value}");
                }
            }
        }
    }
}

