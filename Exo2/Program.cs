﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo2
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] tableauMoyenne = new int[5];
            for (int i = 0; i < 5; i++)
            {

                Console.WriteLine("Veuillez entrer un nombre :");
                string reponse = Console.ReadLine();
                //Besoin de convertir
                int nombre = int.Parse(reponse);
                tableauMoyenne[i] = nombre;
            }
            int moyenneTotale = 0;
            foreach (int item in tableauMoyenne)
            {
                moyenneTotale += item;
            }
            Console.WriteLine("La moyenne est de : " + (moyenneTotale / tableauMoyenne.Length));
            Console.ReadKey();
        }
    }
}
