﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo3
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> liste = new List<int>();
            while (int.TryParse(Console.ReadLine(), out int val))
            {
                liste.Add(val);
            }
            double total = 0;
            foreach (int item in liste) 
            {
                total += item;
            }
            Console.WriteLine(total/liste.Count);
            Console.ReadKey();


            if (File.Exists("Utilisateurs .txt") == false)
            {
                File.Create("Utilisateurs .txt");
            }
            string[] users = File.ReadAllLines("Utilisateurs .txt");
            if (users.Length > 0)
            {
                Console.WriteLine("Liste des utilisateurs : \n");
                foreach (string item in users)
                {
                    Console.WriteLine(item);
                }
            }
            List<string> listeUsers = users.ToList();
            Console.WriteLine("Entrez un utilisateur :");
        }
    }
}
